---
title: "Discord Bot Commands"
output: html_document
---

<hr>

## Mee6

Mee6 commands can be used in most of the discord channels. 

**!help:** Should display a list of commands available to you.

**!levels:** Fetches a link to the server leaderboard, see who spams the most.

**!rank:** Fetches the rank of the person who uses it.

**!fps:** See the common fps values needed to play jumpmod.

**moderator commands:** Use the !help command and you will see what is currently available.

<hr>

## Gamechat

This channels lets you see chat that is happening in the server along with the current map and who is connected. 

**!say:** Talk to the server.

**!maptimes <mapname>:** View the top 15 of a map.

**!playermaps:** View the top 15 players by number of maps completed.

**!playerscores:** View the top 15 players by number of points.

**!1st:** View the first places set today.

**!status:** View who is currently on the server.

<hr>

## Uploading

This bot is meant for mappers only. You probably can't see this channel unless you are a mapper. This bot is meant for uploading beta maps to our test server. There are a few simple rules to do this.

1. Filenames must be lowercase. 

2. Put any custom textures, env, sounds etc in their correct subfolder. For example, if I had a texture named ace.wal in the folder acetexts, I would place it in `textures/acetexts/ace.wal`. 

3. Put all the folders you need into a .zip file and upload it to discord. If the file is too large, use [mediafire](https://www.mediafire.com/) and use a direct link to the file. It should look something like this.

|       .zip format      |
|:----------------------:|
| ![](img/uploading.png) |

4. Finally, use the **!upload** command to upload the map to the test server. You can join the test server from viewing the current IP from the [Server Information](servers.html) page. After joining, use the command `mapvote new` to load your map. Feel free to ask people on the main server or discord to test your map, they are usually more than willing. 

5. Once your map has been tested by multiple people, use the **!final** command to indicate to admins that your map is ready to be uploaded to the main server.

