---
title: "Entities"
output: 
  html_document:
    toc: true
    toc_float:
      collapsed: true
---

Brief descriptions for all entities in quake 2. Click each header for a more descriptive page.

<hr>

## [jump](ents-jump_.html)

| Entity                     | Description                                                            |
|----------------------------|------------------------------------------------------------------------|
| cp_clear                   | clears all the checkpoints of the player                               |
| cpbox_large/medium/small   | adds a single checkpoint to a player                                   |
| jump_cpbrush               | physical wall that can be triggered by checkpoints                     |
| jump_cpwall                | see through wall that can be triggered by checkpoints                  |
| jumpbox_large/medium/small | multi-sized blocks that are usually used by admins to block shortcuts  |
| light_torch                | a lit torch model                                                      |
| one_way_wall               | prevents movement in certain direction while allowing it in others     |
| start_line                 | covers the start area of a map to make the timer not start on movement |
| trigger_finish             | triggers the finish of a run                                           |
| trigger_lapcounter         | counts how many laps a player has done on a map                        |
| trigger_lapcp              | these are needed to be picked up to complete a map                     |
| trigger_quad               | gives the player quad damage                                           |
| trigger_quad_clear         | removes quad damage from the player                                    |
| trigger_single_cp_clear    | clears a single checkpoint from a player                               |

<br>

## [ammo](ents-ammo_.html)

| Entity        | Description                                          |
|---------------|------------------------------------------------------|
| ammo_bullets  | ammo for the machine gun and chaingun                |
| ammo_cells    | ammo for the hyperblaster and BFG                    |
| ammo_grenades | ammo for the grenade launcher and hand grenades      |
| ammo_rockets  | ammo for the rocket launcher                         |
| ammo_shells   | ammo for the single barrel and double barrel shotgun |
| ammo_slugs    | ammo for the railgun                                 |

<br>

## [func](ents-func_.html)

| Entity             | Description                                       |
|--------------------|---------------------------------------------------|
| func_areaportal    | Used to hide areas from the compiler              |
| func_button        | Triggers other entities to act                    |
| func_clock         | An annoying to use clock                          |
| func_door          | A single-plane door                               |
| func_door_rotating | A rotating door                                   |
| func_door_secret   | Doors that look like walls until you shoot them   |
| func_explosive     | Destroyed when triggered                          |
| func_killbox       | Kills everything inside of it                     |
| func_object        | Falls down when triggered                         |
| func_plat          | A moving platform                                 |
| func_rotating      | A rotating platform                               |
| func_timer         | A timing device for other entities                |
| func_train         | A moving object that follows path_corner entities |
| func_wall          | A triggered wall                                  |
| func_water         | A water brush that can move                       |

<br>

## [info](ents-info_.html)

| Entity                   | Description                                    |
|--------------------------|------------------------------------------------|
| info_notnull             | Some uses with lasers and teleports            |
| info_null                | Targeted by a light entity to make a spotlight |
| info_player_coop         | Spawn for a coop game                          |
| info_player_deathmatch   | Spawn for a deathmatch game                    |
| info_player_intermission | Camera view after a level ends                 |
| info_player_start        | Spawn for a single player game                 |

<br>

## [item](ents-item_.html)

| Entity               | Description                                                 |
|----------------------|-------------------------------------------------------------|
| item_adrenaline      | Adds 1 to max health                                        |
| item_ancient_head    | Adds 2 to max health                                        |
| item_armor_body      | Adds 100 armor                                              |
| item_armor_combat    | Adds 50 armor                                               |
| item_armor_jacket    | Adds 25 armor                                               |
| item_armor_shard     | Adds 2 armor                                                |
| item_bandolier       | Increases ammo capacity for weapons                         |
| item_breather        | Provides oxygen underwater                                  |
| item_enviro          | Provides protection from harmful fluids                     |
| item_health          | Adds 10 HP                                                  |
| item_health_large    | Adds 25 HP                                                  |
| item_health_mega     | Adds 100 HP                                                 |
| item_health_small    | Adds 2 HP                                                   |
| item_invulnerability | Temporary invulnerability                                   |
| item_pack            | Adds a large amount of ammo and increases carrying capacity |
| item_power_screen    | Adds 200 armor against energy weapons                       |
| item_power_shield    | Adds armor against energy weapons, drains cells             |
| item_quad            | Adds 4x damage to all weapons                               |
| item_silencer        | Silences the sound of weapons                               |

<br>

## [key](ents-key_.html)

| Entity               | Description    |
|----------------------|----------------|
| key_airstrike_target | Key/checkpoint |
| key_blue_key         | Key/checkpoint |
| key_commander_head   | Key only       |
| key_data_cd          | Key/checkpoint |
| key_data_spinner     | Key/checkpoint |
| key_pass             | Key/checkpoint |
| key_power_cube       | Key/checkpoint |
| key_pyramid          | Key/checkpoint |
| key_red_key          | Key/checkpoint |

<br>

## [light](ents-light_.html)  

| Entity      | Description                                |
|-------------|--------------------------------------------|
| light       | A normal ent light, invisible              |
| light_mine1 | A light from the mine levels, have a shape |
| light_mine2 | A light from the mine levels, have a shape |

<br>

## [misc](ents-misc_.html) 

| Entity               | Description                                                  |
|----------------------|--------------------------------------------------------------|
| misc_banner          | Large flag, use angle to put the face in the right direction |
| misc_bigviper        | Non-moving, massive version of misc_viper                    |
| misc_blackhole       | A roughly 256x256 spinning blackhole                         |
| misc_deadsoldier     | A dead soldier model                                         |
| misc_easterchick     | An easterchick model, uses all 3 models                      |
| misc_easterchick2    | An easterchick model, uses all 3 models                      |
| misc_eastertank      | An easterchick model, uses all 3 models                      |
| misc_explobox        | Barrels that can be pushed, and explode when shot            |
| misc_gib_arm         | Gib piece                                                    |
| misc_gib_head        | Gib piece                                                    |
| misc_gib_leg         | Gib piece                                                    |
| misc_insane          | Insane soldier                                               |
| misc_satellite_dish  | Satellite dish that can be targeted                          |
| misc_strogg_ship     | Flying space ship                                            |
| misc_teleporter      | A teleporter                                                 |
| misc_teleporter_dest | A teleporter destination                                     |
| misc_viper           | Flying space ship                                            |
| misc_viper_bomb      | A droppable bomb for the viper ship                          |

<br>

## [monster](ents-monster_.html)

| Entity                 | Description                                                      |
|------------------------|------------------------------------------------------------------|
| monster_berserk        | a melee strogg<br> health: medium                                |
| monster_boss2          | the tank_boss, but he flies<br> health: very high                |
| monster_boss3          | use monster_jorg instead                                         |
| monster_brain          | a slow, but creepy enemy<br> health: medium                      |
| monster_chick          | a chick with a rocket launcher<br> health: medium                |
| monster_flipper        | a barracuda<br> health: low                                      |
| monster_floater        | a tanky flying menace<br> health: medium                         |
| monster_flyer          | a flying menace<br> health: very low                             |
| monster_gladiator      | a strogg with a rocket launcher<br> health: medium               |
| monster_gunner         | a strogg grenader<br> health: medium                             |
| monster_hover          | a jetpacked menace<br> health: medium                            |
| monster_infantry       | a slightly smarter soldier<br> health: low                       |
| monster_jorg           | an absolute unit of a boss<br> health: ultra high                |
| monster_makron         | use monster_jorg instead                                         |
| monster_medic          | revives fellow strogg<br> health: medium                         |
| monster_mutant         | flying bastards<br> health: medium                               |
| monster_parasite       | an unhelpful dog<br> health: medium                              |
| monster_soldier        | slighty stronger than a solider_light<br> health: low            |
| monster_soldier_light  | a low level mob<br> health: low                                  |
| monster_soldier_ss     | slighter stronger than a soldier<br> health: low                 |
| monster_supertank      | a big baddie with lots of guns<br> health: very high             |
| monster_tank           | a hyperblasting, chaingunning, rocketing madman<br> health: high |
| monster_tank_commander | a fancy tank<br> health: high                                    |

<br>

## [path](ents-path_.html)

| Entity      | Description                       |
|-------------|-----------------------------------|
| path_corner | A travel point for other entities |

<br>

## [point](ents-point_.html)

| Entity       | Description                                          |
|--------------|------------------------------------------------------|
| point_combat | A travel point for other entities before they attack |

<br>

## [target](ents-target_.html)

| Entity                    | Description                                     |
|---------------------------|-------------------------------------------------|
| target_blaster            | Triggered blaster fire                          |
| target_changelevel        | Target for a level change trigger               |
| target_character          | Numbers in a func_clock                         |
| target_crosslevel_target  | Cross level target                              |
| target_crosslevel_trigger | Cross level trigger                             |
| target_earthquake         | Creates an earthquake                           |
| target_explosion          | Creates an explosion                            |
| target_goal               | Just use a target_secret                        |
| target_help               | Makes help messages appear                      |
| target_laser              | Multiple colors of laser                        |
| target_lightramp          | Fades light in and out, very subtle             |
| target_secret             | A secret to find in a map, one time use         |
| target_spawner            | Spawns most items                               |
| target_speaker            | Plays sound                                     |
| target_splash             | Splashes different particles                    |
| target_string             | Used with func_clock                            |
| target_temp_entity        | Spawns some large particle effects and lighting |

<br>

## [trigger](ents-trigger_.html)

| Entity              | Description                                  |
|---------------------|----------------------------------------------|
| trigger_always      | Always on trigger                            |
| trigger_counter     | Multiple input trigger                       |
| trigger_elevator    | Elevator trigger                             |
| trigger_gravity     | Gravity changing trigger                     |
| trigger_hurt        | Damage a player                              |
| trigger_key         | Trigger that works like picking up a key ent |
| trigger_monsterjump | Triggers a monster to jump                   |
| trigger_multiple    | Mutliple time use trigger                    |
| trigger_once        | One time use trigger                         |
| trigger_push        | Push a player in a certain direction         |
| trigger_relay       | Relays actions to other entities             |

<br>

## [turret](ents-turret_.html)

| Entity        | Description                      |
|---------------|----------------------------------|
| turret_base   | Difficult to use turret entities |
| turret_breach |                                  |
| turret_driver |                                  |

<br>

## [weapon](ents-weapon_.html)

| Entity                 | Description                         |
|------------------------|-------------------------------------|
| weapon_bfg             | BFG<br>ammo = cells                 |
| weapon_chaingun        | Chaingun<br>ammo = bullets          |
| weapon_grenadelauncher | Grenade launcher<br>ammo = grenades |
| weapon_hyperblaster    | Hyperblaster<br>ammo = cells        |
| weapon_machinegun      | Machine gun<br>ammo = bullets       |
| weapon_railgun         | Railgun<br>ammo = slugs             |
| weapon_rocketlauncher  | Rocket launcher<br>ammo = rockets   |
| weapon_shotgun         | Shotgun<br>ammo = shells            |
| weapon_supershotgun    | Super shotgun<br>ammo = shells      |

<br>

## [worldspawn](ents-worldspawn_.html)

| Entity     | Description                         |
|------------|-------------------------------------|
| worldspawn | Default entity for a map to compile |

