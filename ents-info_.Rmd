---
title: "Info Entities"
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float:
      collapsed: false
---

<hr>

| Entity                   | Description                                    |
|--------------------------|------------------------------------------------|
| info_notnull             | Some uses with lasers and teleports            |
| info_null                | Targeted by a light entity to make a spotlight |
| info_player_coop         | Spawn for a coop game                          |
| info_player_deathmatch   | Spawn for a deathmatch game                    |
| info_player_intermission | Camera view after a level ends                 |
| info_player_start        | Spawn for a single player game                 |

<br><br>

## info_notnull

<hr>

This ent has a two main ways of using it. The first is as a target for [target_lasers](ents-target_.html#target_laser). By default lasers only shoot vertical or horizontal, targeting an info_notnull allows them to shoot in any direction. 

The second use is as a replacement for a [misc_teleporter_dest](ents-misc_.html#misc_teleporter_dest). The advantage of an info_notnull as a destination is that the player does not spawn on a platform, meaning the player can free-fall immeadiately after, or spawn in mid-air.

<br><br>

## info_null

<hr>

The only use for this ent is when coupled with a [light](ents-light_.html#light) ent. It is used to create a spotlight at the location of the info_null. Please see [light](ents-light_.html#light) for more on how this is done.

<br><br>

## info_player_coop

<hr>

Spawn for a coop game with a pad underneath it.

<br>

#### keypairs

**angle:** the angle the player will spawn in at

**targetname:** can be targeted by a teleporter with this name

<br><br>

## info_player_deathmatch

<hr>

Spawn for a deathmatch game with a pad underneath it.

<br>

#### keypairs

**angle:** the angle the player will spawn in at

**targetname:** can be targeted by a teleporter with this name

<br><br>

## info_player_intermission

<hr>

This is the camera view you see at the end of some maps. This ent uses the **angles** keypair to orient a camera in any direction you choose using a y(pitch), z(yaw), x(roll) format. Pitch controls the up/down angle, with 0 being neutral. Yaw works like a normal angle keypair, and corresponds to the topdown view in an editor. Roll controls the side to side tilt of the camera. A 90 degree tilt would put the player on his side. 

<br>

#### keypairs

**angles:** in y(pitch) z(yaw) x(roll) format (ie. 30 225 0)

<br><br>

## info_player_start

<hr>

Technically used for a single player game, but is also the preferred spawn for jumpmod. Unlike the info_player_deathmatch, it has no pad underneath it. 

<br>

#### keypairs

**angle:** the angle the player will spawn in at

**targetname:** can be targeted by a teleporter with this name
