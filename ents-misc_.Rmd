---
title: "Misc Entities"
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float:
      collapsed: false
---

<hr>

| Entity               | Description                                                  |
|----------------------|--------------------------------------------------------------|
| misc_banner          | Large flag, use angle to put the face in the right direction |
| misc_bigviper        | Non-moving, massive version of misc_viper                    |
| misc_blackhole       | A roughly 256x256 spinning blackhole                         |
| misc_deadsoldier     | A dead soldier model                                         |
| misc_easterchick     | An easterchick model, uses all 3 models                      |
| misc_easterchick2    | An easterchick model, uses all 3 models                      |
| misc_eastertank      | An easterchick model, uses all 3 models                      |
| misc_explobox        | Barrels that can be pushed, and explode when shot            |
| misc_gib_arm         | Gib piece                                                    |
| misc_gib_head        | Gib piece                                                    |
| misc_gib_leg         | Gib piece                                                    |
| misc_insane          | Insane soldier                                               |
| misc_satellite_dish  | Satellite dish that can be targeted                          |
| misc_strogg_ship     | Flying space ship                                            |
| misc_teleporter      | A teleporter                                                 |
| misc_teleporter_dest | A teleporter destination                                     |
| misc_viper           | Flying space ship                                            |
| misc_viper_bomb      | A droppable bomb for the viper ship                          |

<br>

## misc_banner

<hr>

A wavy flag that is roughly 128 units tall. Set a `angle` keypair to rotate it to fit the wall it is placed on. If it appears darker than it should, rotate it 180 degrees. 

<br><br>

## misc_bigviper

<hr>

A simple one to use, it has no keypairs or options. It's just a large ship model that can be placed anywhere in your map.

<br><br>

## misc_blackhole

<hr>

A roughly 256x256 sized ent that looks kind of neat. There are no options or flags to worry about.

<br><br>

## misc_deadsoldier

<hr>

A dead soldier model that can be changed via spawnflags. 

<br>

#### spawnflags

**1 on_back**

**2 on_stomach**

**4 back_decap**

**8 fetal_pos**

**16 sit_decap**

**32 impaled**

<br><br>

## misc_easterchick

<hr>

This goes along with the misc_easterchick2 and misc_eastertank ents. A few quick steps to use these. First place a misc_eastertank on the floor. Then place a misc_easterchick so that it's bottom side is aligned with the bottom side of the misc_eastertank. Finally put the misc_easterchick2 in the same place as the misc_easterchick. 

| easterchick placement |
|:---------------------:|
|  ![](img/easter.jpg)  |

<br><br>

## misc_explobox

<hr>

Barrels that can be pushed around and explode when they are shot.

<br><br>

## misc_gib

<hr>

This includes the misc_gib_arm, misc_gib_leg, and misc_gib_head ents. This is often used along side with a [target_spawner](ents-target_.html#target_spawner). 

<br><br>

## misc_insane

<hr>

These are the wandering marines you see in the single player campaign. Here are the spawnflags on how to control them.

<br>

#### spawnflags

**1 ambush:** The insane marine will not react to you unless it has a direct line of sight. It will then follow you around unless a path is set.

**2 trigger_spawn:** If set, you must give the marine a targetname and have something else trigger it's spawn. The typical usage is with a [target_spawner](ents-target_.html#target_spawner). 

**4 crawl:** This will cause the marine to crawl.

**8 crucified:** A crucified marine that can be used as a decoration.

**16 stand_ground:** The marine will stand in place and never move. 

**32 always_stand:** The marine will not drop to it's knees.

<br><br>

## misc_satellite_dish

<hr>

A satellite dish model that can be used with or without being triggered. If not triggered it will be a simple model. If triggered, it will tilt up, and then to the left. If you choose to trigger it, the animation for going back to the original position is not great, so it is best to use a [trigger_once](ents-trigger_.html#trigger_once) or a [func_button](ents-func_.html#func_button) with a `wait` value of -1 (never return).

<br><br>

## misc_strogg_ship

<hr>

Along with the misc_viper, these are the space ships that you see in some of the vanilla levels flying through the sky. They function in a similar way to a [func_train](ents-func_.html#func_train).

To get them working, you will first need to place the ship entity anywhere in the level. Then make [path_corner](ents-path_.html#path_corner) ents for the route you wish the ship to travel. Give the ship a target value to the first path_corner. 

To keep a ship looping on the path, you will need to set a `teleport` spawnflag for the final path_corner and make sure it connects back to the first path_corner. 

The last step is to create a trigger of any kind to turn the ship on. To always have the ship running use a [trigger_always](ents-trigger_.html#trigger_always). Trigger the misc_viper or strogg_ship with the trigger_always.

<br>

#### keypairs

**speed:** default 300, the speed that the ship will travel

**angle:** the angle the ship wall face

<br><br>

## misc_teleporter

<hr>

This just teleports a player to a destination. There are a few other variants and options for a teleporter in jumpmod, but those mainly are used with a [trigger_teleport](ents-trigger_.html#trigger_teleport). 

<br>

#### keypairs

**target:** the name of the misc_teleport_dest that you want to travel to

<br><br>

## misc_teleporter_dest

<hr>

The landing zone for a misc_teleporter. It is recommended to place this 1 unit under the floor of your map in order to hide the platform it produces. An alternative ent that does not have a base like this ent does is an [info_notnull](ents-info_.html#info_notnull). 

#### keypairs

**targetname:** the name of the teleport destination

<br><br>

## misc_viper_bomb

<hr>

The first step to learning this is ent is to learn how to use the [misc_viper](ents-misc_.html#misc_viper) ent. 

The viper bomb must be placed at one of the path_corners of the misc_viper ent route. The bomb arc just like a real bomb, so it should be placed before the intended target. The bomb will travel horizontally roughly equal to half of it's height. 

The path_corner the bomb is placed on must be given a keypair for `pathtarget` that matches the viper_bomb targetname and pathtarget. For simplicity give it the name `boom`.

<br>

#### keypairs

**dmg:** Default 1000, the damage the bomb will do to things around it. 

**targetname:** give it the name `boom`

**pathtarget:** give it the name `boom`