---
title: "Trigger Entities"
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float:
      collapsed: false
---

<hr>

| Entity              | Description                                  |
|---------------------|----------------------------------------------|
| trigger_always      | Always on trigger                            |
| trigger_counter     | Multiple input trigger                       |
| trigger_elevator    | Elevator trigger                             |
| trigger_gravity     | Gravity changing trigger                     |
| trigger_hurt        | Damage a player                              |
| trigger_key         | Trigger that works like picking up a key ent |
| trigger_monsterjump | Triggers a monster to jump                   |
| trigger_multiple    | Mutliple time use trigger                    |
| trigger_once        | One time use trigger                         |
| trigger_push        | Push a player in a certain direction         |
| trigger_relay       | Relays actions to other entities             |

<br>

## trigger_always

<hr>

A trigger that is always on, and starts when the level begins. 

<br>

#### keypairs

**target:** the thing you are triggering

**killtarget:** after triggering the target once, it destroys the ent

**delay:** the time between firings

<br><br>

## trigger_counter

<hr>

A trigger that allows for multiple inputs. This means the trigger will only fire when multiple different inputs are satisfied. As an example, if you want a door to open only when two different buttons are pressed. First make the door and give it a `targetname` of `door1`. Then make a trigger_counter and set the `target` to `door1`, `targetname` to `counter1`, and `count` to `2`. Now create two func_buttons and set their `target` to `counter1`. 

<br>

#### keypairs

**target:** the thing you are triggering

**targetname:** the name of the trigger

**count:** the number of actions that are required to fire the trigger

<br>

#### spawnflags

**1 nomessage:** typically the counter will display how many items are left, this will disable those

<br><br>

## trigger_elevator

<hr>

This is by far one of the more annoying entities to use and setup, but provides a pretty neat function once you do. It is as the name implies, an elevator. You can setup buttons at each floor and call the elevator to it. Before you start making the elevator, you must know how each of the following work, and be comfortable using them:

- [func_train](ents-func_.html#func_train)

- [path_corner](ents-path_.html#path_corner)

- [func_button](ents-func_.html#func_button)

<br>

#### Usage

1. Place a trigger_elevator somewhere in the map. This is not the elevator platform, and will not be visible. Give the trigger_elevator a `targetname` of `elevator` and a `target` of `train`.

2. Now create the actual elevator platform. This can be a simple square or something more elaborate. When you are finished turn the entire platform into a func_train. Give the func_train the `targetname` of `train` so it will be targetted by the trigger_elevator. Then give it a `target` of `p1`, which will be the first path_corner. 

3. Now create the actual path_corner and give it a `targetname` of `p1` to link it to the func_train. Place this on the lowest floor the elevator will go to. Repeat this for each floor, and give each one a `targetname` according to the floor, `p2`, `p3`, `p4`, etc. 

4. Each path_corner needs a `pathtarget` keypair. Give each one a name like `stop1` for each floor. 

5. Now we need to create a func_button for each floor. Give each button a `target` of `elevator` as that is what we named the trigger_elevator. 

6. Each func_button also needs a `pathtarget` that you want the elevator to stop on when pressed. For instance, for the bottom func_button on floor 1, you will want to make the `pathtarget` set to `p2` as that is the name of the 2nd floor path_corner. Then so and so forth. Basically, you want to set the `path_target` of each floor to the floor you want it to go to next.

7. If you want to setup buttons to call the elevator to your location, set a func_button's `pathtarget` to floor the button is on. 

<br><br>

## trigger_gravity

<hr>

This is a trigger that affects the gravity of the level. In vanilla quake 2, this only affects non-player entities. However, in jumpmod it was modified to work with players, and not entities. 

Using it is very simple, just create a brush the size of the area you want to change gravity in. Alternatively, you can create the trigger over the exit a teleporter or in a confined space the player has to pass through. A downside of this trigger is that gravity does not reset when the player respawns. To combat this, just place another trigger_gravity over the player spawn. The default gravity you need to set is 800.

<br>

#### keypairs

**gravity:** default 800, it can be set negative to make a player float up

<br><br>

## trigger_hurt

<hr>

A trigger that hurts the player when they enter it. 

<br>

#### keypairs

**dmg:** Default 5, how much damage the trigger will do.

<br>

#### spawnflags

**1 start_off:** the trigger doesn't work until triggered on

**2 toggle:** the trigger can be turned on and off

**4 silent:** supresses the sounds the trigger makes

**8 no_protection:** damage goes directly to health, even if the player has armor, or any other form of protection

**16 slow:** the damage ticks are slower

<br><br>

## trigger_key

<hr>

This is used in combination with [key entities](ents-key_.html). It is used to gate a door or trigger behind the player having a specific key. It does not work in jumpmod as the keys have been reworked to use as checkpoints instead. Using this item is a little counterintuitive, so an example is the easiest way. We will use a key_data_spinner to stop a fan.

1. Make the fan using a [func_rotating](ents-func_.html#func_rotating) and give it a `targetname` of `fan`.

2. Place a trigger_key somewhere and give it a `target` of `fan`. Also give it a `targetname` of `key`. 

3. Now give the trigger_key the `item` keypair with the value `key_data_spinner`. 

4. Wherever you want the player to bring the key to must have a trigger that triggers the trigger_key. In this case, set the `target` to `key`.

<br>

#### keypairs

**item:** used to specify the key you need, the options are on the key page above

<br><br>

## trigger_monsterjump

<hr>

An entity that can only be triggered by monster. It will make the monster jump in the specified direction, speed, and height. 

<br>

#### keypairs

**angle:** the angle that the monster will jump

**speed:** default is 200, the speed thrown in the angle from above

**height:** default is 200, the height thrown upward

<br><br>

## trigger_multiple

<hr>

A trigger that can be fired multiple times. Used extensively along with trigger_once for every single map. It is the basis of many contraptions and has many different types of uses. 

<br>

#### keypairs

**target:** the name of the ent this is triggering

**targetname:** used if also using the `triggered` spawnflag, as it must be triggered into existance by another ent

**sounds:** the sound that plays when triggered; 1 = `secret`, 2 = `beepbeep`, 3 = `large switch`

**message:** a message to display to players

**delay:** this is the time in seconds between touching the trigger and it firing

**killtarget:** will remove any entity that has a matching targetname

**wait:** default is 0.2, amount of time between firings

<br>

#### spawnflags

**1 monster:** allows monster and players to use the trigger

**2 not_player:** allows only monsters to use the trigger

**4 triggered:** the trigger must be triggered into existance before it can be used

<br><br>

## trigger_once

<hr>

Similar to a trigger_multiple, but only fires once before removing itself from the world. Refer to trigger_multiple on how to use this. The only differences are that the keypair `wait`, and the spawnflag `monster` do not function.

<br><br>

## trigger_push

This is used to push the player in any direction.

<br>

#### keypairs

**speed:** default 1000, the speed at which you are launched

**angle:** -1 for up, -2 for down, the rest are normal

**angles:** you can also set a 3d angle much like in [info_player_intermission](ents-info_.html#info_player_intermission) which means you can send players at diagonals. A great example of this is in the castlejumps map.

<br>

#### spawnflags

**1 push_once:** the ent only pushes one time, and then deletes itself from the map

<br><br>

## trigger_relay

<hr>

Used to relay actions. It cannot be triggered by a player walking through it, which means it must have both a `target` and `targetname`. An example of use for this ent is to have a crate, that when shot, causes other nearby crates to explode. You would create a [func_explosive](ents-func_.html#func_explosive) crate, and give it a `target` that targets a trigger_relay. The relay would then target multiple other func_explosive crates.

<br>

#### keypairs

**target:** the ent the trigger_relay is triggering

**targetname:** the ent that triggers the relay

**delay:** the amount of time between being triggered and firing

**killtarget:** permanently destroy the ent listed in the `target` keypair

**message:** the message that displays when fired
