---
title: "Config Generator"
output: html_document
---

Below is a simple config for a jump server. You can edit the fields to what you need. I used my own config for this and tried to keep only essentials, so some values may not be ideal for you. Just delete all your old configs and place this in your mod folder. Feel free to add lines anywhere with additional binds or settings you may want. I included a lot of FPS binds to give some examples of different options, delete the ones you don't like or don't need.



<html>
<body>
 
<table>
    <tr><td>autoexec.cfg:</td></tr>
    <tr>
        <td colspan="3">
            <textarea id="inputTextToSave" style="color:black;" cols="80" rows="25">
// basics
set name "your_name_here" your name
set sensitivity "3.6" your sensitivity
set hand "2" // 0 = gun right, 1 = left, 2 = invisible
set fov "120" // your fov
set cl_maxfps "120" // your default FPS
set crosshair "1" // set a default crosshair
set cl_run "1" // 1 = always run

// video settings
set gl_modulate "3" // default brightness
set intensity "1.85" // default intensity of textures
set vid_gamma "1" // default gamma
set vid_forcewidth "1920" // set to width of your monitor
set vid_forceheight "1080" // set to height of your monitor
set gl_zfar "100000" // sets a nearly infinite sight distance
set gl_swapinterval "0" // leave at 0
set gl_dynamic "1" // turns dynamic lights on or off

// sound settings
set s_volume "0" // 0 for no sound, 1 for sound

// movement and jump settings
bind w "+forward" // move forward
bind s "+back" // move backward
bind a "+moveleft" // move left
bind d "+moveright" // move right
bind CTRL "+movedown" // crouch
bind MOUSE2 "+moveup" // jump
bind MOUSE1 "+attack" // attack
bind f "store" // store your location
bind SPACE "kill" // return to your location
bind g "noclip" // enter noclip
bind h "+hook" // use the hook
bind ALT "" // unbind default alt behavior

// FPS settings, set keys for fps binds
bind 1 "cl_maxfps 120"
bind MWHEELUP "cl_maxfps 120"
bind 2 "cl_maxfps 90"
bind MOUSE5 "cl_maxfps 90"
bind 3 "cl_maxfps 66"
bind MOUSE3 "cl_maxfps 66"
bind q "cl_maxfps 34"
bind 4 "cl_maxfps 27"
bind e "cl_maxfps 27"
bind MWHEELDOWN "cl_maxfps 27"
bind 5 "cl_maxfps 22"
bind r "cl_maxfps 22"
bind MOUSE4 "cl_maxfps 20"

// menu settings
bind TAB "inven" // bring up the team and observer menu
bind ENTER "invuse" // select current menu item
bind UPARROW "invprev" // select menu item above
bind DOWNARROW "invnext" // select menu item below
bind ` "toggleconsole" // open or close console
bind ESCAPE "togglemenu" // toggle client menu
bind F1 "cmd help" // show the jump menu

// messaging settings
bind t "messagemode" // say to game
bind y "messagemode2" // say to team

// download settings, allow all downloads
set allow_download "1"
set allow_download_players "1"
set allow_download_pics "1"
set allow_download_maps "1"
set allow_download_sounds "1"
set allow_download_models "1"

//servers
set adr0 "46.165.236.118:27910" // german jump main
set adr1 "46.165.236.118:28910" // german test server
set adr2 "37.120.173.145:11120" // arctic jump
set adr3 "54.38.54.130:27910" // PL Jump
set adr4 "23.227.170.222:27940" // TS Jump
set adr5 "" // empty</textarea>
        </td>
    </tr>
    <tr>
        <td>Filename to Save As:</td>
        <td><input id="inputFileNameToSaveAs" style="color:black;" value = "autoexec.cfg"></input></td>
        <td><button style="color:black;" onclick="saveTextAsFile()">Save Text to File</button></td>
    </tr>
    <tr>
        <td>Select a File to Load:</td>
        <td><input type="file" id="fileToLoad"></td>
        <td><button style="color:black;" onclick="loadFileAsText()">Load Selected File</button><td>
    </tr>
</table>
 
<script type="text/javascript">
 
function saveTextAsFile()
{
    var textToSave = document.getElementById("inputTextToSave").value;
    var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
    var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
    var fileNameToSaveAs = document.getElementById("inputFileNameToSaveAs").value;
 
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    downloadLink.href = textToSaveAsURL;
    downloadLink.onclick = destroyClickedElement;
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
 
    downloadLink.click();
}
 
function destroyClickedElement(event)
{
    document.body.removeChild(event.target);
}
 
function loadFileAsText()
{
    var fileToLoad = document.getElementById("fileToLoad").files[0];
 
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) 
    {
        var textFromFileLoaded = fileLoadedEvent.target.result;
        document.getElementById("inputTextToSave").value = textFromFileLoaded;
    };
    fileReader.readAsText(fileToLoad, "UTF-8");
}
 
</script>
 
</body>
</html>

<hr>
<br><br>

Some examples for additional binds could be:

| Command        | Description                                   |
|----------------|-----------------------------------------------|
| replay         | replay the first place time on a map          |
| reset          | removes your store location                   |
| recall 2 or 3  | go to your 2nd or 3rd previous store location |
| velstore       | toggle storing velocity with store            |
| yes / no       | for quickly voting                            |
| votetime 5     | quickly add 5 minutes to a map                |
| mapvote random | randomly vote a new map                       |