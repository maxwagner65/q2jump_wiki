---
title: "Corona Cup 2020"
output: 
  html_document:
    toc: true
    toc_depth: 5
    toc_float:
      collapsed: true
---

<center>
![](img/ccup2020.png)
</center>


### Overview

<hr>

We are all home and bored, so it feels like a good time to host a tournament. This is going to be a combination of a mapping and jumping tournament. A general outline of the tournament is below.

* Mapping contest maps are made
* Jump tournament is played on the new maps
* Mapping contest maps are judged
* Sign-ups are on our [Discord](https://discord.q2jump.net)
* If you are new, and want to try to get involved, ask us questions on discord to get set up.

**Current Prizepool:** Total: $500 --- Ace: $200, Goblin: $100, Opkez: $100, Koala: $50, Caboom: $50

<br><br>

### Jump Tournament

<hr>

Twenty minute time limits for each map, played live. All the maps will be brand new. It will start with two heats, with the top four from each moving directly to the finals. The remaining people will go to a last chance qualifier, where the top two will move to the finals. In the finals, the top five will get a prize. The points distribution is below.

| Place | Points | Place | Points |
|-------|--------|-------|--------|
| 1     | 15     | 6     | 5      |
| 2     | 12     | 7     | 4      |
| 3     | 10     | 8     | 3      |
| 4     | 8      | 9     | 2      |
| 5     | 6      | 10    | 1      |

<br>

### Results

<hr>

Here are the results of the tournament. The people who advanced have a * next to their name in the heats and LCQ. In the finals we had Goblin take 1st, Killa take 2nd, ace take 3rd, slip take 4th, and yogurt take 5th. APKIS, prd, bossmies, neox, and opkez rounded out the remaining finalists. We had an incredible turn out for this being our first tournament in years with some good games and great times. Thanks to everyone old and new who showed up to make this a success. 

<br>

##### Heat 1

| pos | player        | cc_neox1 | cc_ace1 | cc_mehis | cc_mako | total |
|-----|---------------|----------|---------|----------|---------|-------|
| 1   | goblin*        | 1        | 1       | 1        | 2       | 57    |
| 2   | APKIS*         | 3        | 2       | 2        | 1       | 49    |
| 3   | prd*           | 4        | 3       | 3        | 3       | 38    |
| 4   | 754*           | 2        | 4       | 4        | 5       | 34    |
| 5   | bossmies       | 8        | 5       | 6        | 4       | 22    |
| 6   | barry_allen    | 6        | 6       | 9        | 6       | 17    |
| 7   | mazze          | 7        | 9       | 5        | 7       | 16    |
| 8   | zerosignal     | 5        | 8       | 7        | 9       | 15    |
| 9   | direktor       | 9        | 7       | 8        | 8       | 12    |
| 10  | gouki          | 10       | 10      | 10       |         | 3     |
| 11  | Woeste Wessel  | 11       |         |          |         | 0     |

<br>

##### Heat 2

| pos | name   | cc_slip2 | cc_zero1 | cc_play | cc_direktor1 | total |
|-----|--------|----------|----------|---------|--------------|-------|
| 1   | opkez*  | 4        | 6        | 1       | 1            | 43    |
| 2   | killa*  | 3        | 2        | 4       | 2            | 42    |
| 3   | yogurt* | 2        | 1        | 5       | 5            | 39    |
| 4   | ace*    | 1        | 3        | 7       | 3            | 39    |
| 5   | mehis  | 8        | 4        | 3       | 8            | 24    |
| 6   | kallu  | 6        | 9        | 2       | 7            | 23    |
| 7   | zod    | 9        | 5        | 8       | 4            | 19    |
| 8   | caboom | 7        | 8        | 9       | 6            | 14    |
| 9   | neox   | 5        |          | 6       | 11           | 11    |
| 10  | sluggy | 10       | 7        | 11      | 9            | 7     |
| 11  | mako   | 11       | 10       | 10      | 10           | 3     |

<br>

##### LCQ

| pos | name          | cc_slip1 | cc_opkez | cc_mako | cc_phx1 | cc_ace2 | cc_slip2 | total |
|-----|---------------|----------|----------|---------|---------|---------|----------|-------|
| 1   | neox*         | 4        | 1        | 2       | 5       | 6       | 3        | 56    |
| 2   | bossmies*     | 8        | 4        | 1       | 6       | 1       | 6        | 51    |
| 3   | mazze         | 6        | 3        | 3       |         | 3       | 2        | 47    |
| 4   | barry_allen   | 1        | 10       | 5       | 7       | 4       | 9        | 36    |
| 5   | zod           | 3        | 12       | 4       | 3       | 8       | 11       | 31    |
| 6   | caboom        |          | 8        | 6       | 9       | 5       | 1        | 31    |
| 7   | mehis         | 5        | 7        | 8       | 4       | 7       | 5        | 31    |
| 8   | direktor      | 9        | 2        | 12      | 1       | 13      | 12       | 29    |
| 9   | kallu         |          | 11       | 7       |         | 2       | 4        | 24    |
| 10  | zerosignal    | 10       | 5        | 11      | 2       | 11      | 7        | 23    |
| 11  | play          | 2        | 9        | 10      |         | 10      | 8        | 19    |
| 12  | mako          | 7        | 6        | 9       | 8       | 9       | 10       | 17    |
| 13  | gouki         |          | 13       | 13      |         | 12      | 13       | 0     |
| 14  | Woeste Wessel |          |          |         |         | 14      |          | 0     |

<br>

##### Final

| pos | name     | cc_mako2 | cc_direk2 | cc_zero2 | cc_neox2 | cc_slip3 | cc_barry1 | total | prize |
|-----|----------|----------|-----------|----------|----------|----------|-----------|-------|-------|
| 1   | goblin   | 1        | 1         | 4        | 1        | 1        | 1         | 83    | $100  |
| 2   | killa    | 5        | 2         | 1        | 10       | 5        | 2         | 52    | $63   |
| 3   | ace      | 8        | 5         | 2        | 3        | 3        | 5         | 47    | $50   |
| 4   | 754      | 10       | 6         | 3        | 5        | 2        | 6         | 39    | $25   |
| 5   | yogurt   | 3        | 4         | 6        | 7        | 4        | 9         | 37    | $13   |
| 6   | APKIS    | 2        | 7         | 5        | 6        | 8        | 10        | 31    |       |
| 7   | prd      | 9        | 3         | 10       | 8        | 6        | 3         | 31    |       |
| 8   | bossmies | 4        | 8         | 9        | 9        | 7        | 4         | 27    |       |
| 9   | neox     | 7        | 10        | 7        | 2        |          | 8         | 24    |       |
| 10  | opkez    | 6        | 9         | 8        | 4        | 9        | 7         | 24    |       |

<br><br>

### Mapping Contest

<hr>

The mapping contest is very open ended with only a few limitations. These maps will be used to fill the mappool for the tournament following it. They should be designed around the idea of playing them for 20-30 minutes. Mappers can submit as many suitable maps as they want. The constraints are:

* The approximate **perfect** time is between 20 and 60 seconds
* The difficulty should be anywhere from easy to medium, nothing super hard
* Any style of map is fine, ie. ice, tech, strafe, tower maps
* Judging will be based on gl_modulate 3, intensity 1.85
  
<br>

#### Judging:

The judging will be done via a survey of players who participated in the cup and people who have played the maps on the main server. You can find a link to previews of the maps [here](ccupmaps.html).

| Category | Points | Description                     |
|----------|--------|---------------------------------|
| Design   | 10     | How does it look?               |
| Gameplay | 10     | How does it play competitively? |
| Total    | 20     |                                 |












