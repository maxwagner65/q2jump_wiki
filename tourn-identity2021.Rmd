---
title: "Q2 Identity Cup"
output: 
  html_document:
    toc: true
    toc_depth: 5
    toc_float:
      collapsed: true
---

<center>
![](img/identitycup/q2identity.png)
</center>

### Results

<hr>

Congratulations to the winners of the tournament. The jumping/mapping contest winners are below. You can see a recap of the maps on my youtube [here](https://www.youtube.com/watch?v=FVy2MhQRrVw){target="_blank"}. Thanks to all the players, mappers, and donors that helped make it work. It still amazes me we have people around after 20+ years. You can see screenshots of the maps [here](tourn-identity-maps.html). See you guys in 2030 for the next one.

|   | Jumping | Prize | Mapping    | Prize |
|---|---------|-------|------------|-------|
| 1 | Goblin  | 125   | zerosignal | 250   |
| 2 | Mazze   | 75    | direktor   | 150   |
| 3 | Mehis   | 50    | kallu      | 100   |

<br><br>

### Overview

<hr>

This will be a traditional q2 jump/mapping tournament, with a few twists to make it interesting.

* It will start on Saturday December 11th.
  * Group games will be on the 11th/12th
  * LCQ will be on the 18th
  * Main final will be on the 19th.
  * Start time for games will be roughly 12-3pm for US, 6-9pm for Euros
  
* New maps made by the community that all feature an <mark style="background-color: #2a9fd6">identity</mark>
  * Maps are due to admins the day before the game they'll be played in
  * Mapping contest maps are judged by game play and creativity
  
* Sign-ups are on our [Discord](https://discord.q2jump.net)

<hr>

**Current Prize pool:** - $750

| Pos | Jumping | Mapping |
|-----|---------|---------|
| 1   |    $125 |    $250 |
| 2   |     $75 |    $150 |
| 3   |     $50 |    $100 |

<br><br>

### Jump Tournament

<hr>

The jump tournament will be played on the new maps made specifically for this event. 

* Group stage:
  * Finish in the top few places to go directly to the final
  * Every one else will go to a last chance qualifier
  * Short time limit, all new maps

* LCQ:
  * Finish in the top 2 places to move to the main final
  * Short time limit, all new maps
  
* Final:
  * Top 6 seeds will each pick a map to play
  * Longer time limit than group games
  * Payout to the top 5

<br>

#### Points

| Place | Points | Place | Points |
|-------|--------|-------|--------|
| 1     | 15     | 6     | 5      |
| 2     | 12     | 7     | 4      |
| 3     | 10     | 8     | 3      |
| 4     | 8      | 9     | 2      |
| 5     | 6      | 10    | 1      |

<br><br>

### Mapping Contest

<hr>

The mapping contest is the twist this time around. Each map will feature an <mark style="background-color: #2a9fd6">identity</mark>. An identity is generally a defining characteristic of the map. This could either be a unique environment, a creative jump, or anything else setting it apart from other maps. The idea behind this is to create distinctive maps that people remember. Here are some examples. Further guidelines are below. Here are some [examples](tourn-identity-examples.html).

- The map *must* have an identity of some kind, this could be:
  - a unique setting/theme
  - a creative jump/sequence of jumps
  - anything similar to the link to examples above
  
- An identity is **NOT**:
  - the entirety of the map is a gimmick, IE the aids/gay series of maps
  - a random jump, IE, moving blocks, invisible walls, difficult to replicate glitches
  
- The approximate *perfect* time is between 20 and 45 seconds
  - this is for a perfect time, meaning not everyone will finish within the 45 second limit
  - 45 seconds is a HARD cutoff, meaning the map won't be included if it goes over the limit
  - you are free to make a longer/harder version after the tournament
  
- The difficulty should be anywhere from easy to medium
  - ideally the maps should be completable by most players
  - if you wish to include a hard jump, perhaps include a longer, easier side path
  - examples of difficulty to shoot for would be, the ccup maps, ariejumps, adrenjumps, 4jumps
  
- Any style of map is fine, ie. ice, tech, strafe, tower maps
  - the only exception would be pure flat strafe, as it would likely violate the identity rule
  - no *secret* routes, or hidden shortcuts, normal shortcuts are fine
  - the route of the map should be relatively understandable, these have to be competitive for 20 minute matches
  
- Mappers are free to submit multiple maps, but they must be in different styles / identities
  - mappers can participate in the jump tournament, but they won't play their own maps in group stages
  - Wait until either myself, zero, or direktor contacts you to hand your map in

- Judging will be based on gl_modulate 3, intensity 1.85, vid_gamma 1
  - mappers will have until dec 31st to send a final copy to me (ace)
  - this time period is to allow mappers to finalize their maps after they are played in the tournament
  - voting will start on jan 1st, and go until we have enough responses
  
<br>

#### Judging

The judging will be done via a survey of players who participated in the cup and people who have played the maps on the main server. 

| Category     | Points | Description                               |
|--------------|--------|-------------------------------------------|
| Creativity   | 10     | How creative is the identity/overall map? |
| Game play    | 10     | How does it play competitively?           |












