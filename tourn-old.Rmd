---
title: "Past Tournaments"
---

A list of prior tournaments. Any ongoing events or tournaments will be in the nav bar.

* 2021 - [id Cup Tournament](tourn-identity2021.html)
* 2021 - [id Cup Map Previews](tourn-identity-maps.html)
* 2020 - [Corona Cup Tournament](tourn-ccup2020.html)
* 2020 - [Corona Cup Map Previews](tourn-ccupmaps.html)
* Misc. - [Q2 Scene Tournaments](http://q2scene.net/jump/index.php)

